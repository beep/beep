;;;; -*- mode: lisp; package: beep-uffi -*-

;;;; Interface to the Beep Media Player
;;;; Copyright (C) 2004 Matthew Kennedy <mkennedy@gentoo.org>

;;;; Commentary:

;;;; Code:

(in-package #:beep-uffi)

(defmacro define-beep-function (name &optional args (returning :void))
  (flet ((make-name-string ()
	   (if (atom name)
	       (list (concatenate 'string 
				  "xmms_remote_" 
				  (substitute #\_ #\- (string-downcase (symbol-name name)) :test #'char=))
		     name)
	       name)))
    `(def-function ,(make-name-string)
	 ((session :int)
	  ,@args)
       :module "beep"
       :returning ,returning)))

;; playlist
(define-beep-function get-version () :void)
(define-beep-function play)
(define-beep-function pause)
(define-beep-function stop)
(define-beep-function is-playing () :int)
(define-beep-function is-paused () :int)
(define-beep-function get-playlist-pos () :int)
(define-beep-function set-playlist-pos ((pos :int)))
(define-beep-function get-playlist-length () :int)
(define-beep-function playlist-clear () :int)
(define-beep-function get-output-time () :int)
(define-beep-function jump-to-time ((pos :int)))
(define-beep-function ("xmms_remote_get_volume" %get-volume) ((left (* :int)) (right (* :int))))
(define-beep-function get-main-volume () :int)
(define-beep-function get-balance () :int)
(define-beep-function set-volume ((left :int) (right :int)))
(define-beep-function set-main-volume ((level :int)))
(define-beep-function set-balance ((balance :int)))
(define-beep-function get-skin () :cstring)
(define-beep-function set-skin ((skin-file :cstring)))
(define-beep-function get-playlist-file ((pos :int)) :cstring)
(define-beep-function get-playlist-title ((pos :int)) :cstring)
(define-beep-function get-playlist-time ((pos :int)) :int)
(define-beep-function ("xmms_remote_get_info" %get-info) ((rate (* :int)) (freq (* :int)) (nch (* :int))))
(define-beep-function main-win-toggle ((show :int)))
(define-beep-function pl-win-toggle ((show :int)))
(define-beep-function eq-win-toggle ((show :int)))
(define-beep-function show-prefs-box)
(define-beep-function toggle-aot ((ontop :int)))
(define-beep-function eject)
(define-beep-function playlist-prev)
(define-beep-function playlist-next)
(define-beep-function playlist-add-url-string ((string :cstring)))
(define-beep-function is-running () :int)
(define-beep-function toggle-repeat)
(define-beep-function toggle-shuffle)
(define-beep-function is-repeat () :int)
(define-beep-function is-shuffle () :int)
(define-beep-function get-eq-preamp () :float)
(define-beep-function get-eq-band ((band :int)) :float)
(define-beep-function set-eq-preamp ((preamp :float)))
(define-beep-function set-eq-band ((band :int) (value :float)))
(define-beep-function quit)
(define-beep-function play-pause)
(define-beep-function playlist-ins-url-string ((string :cstring) (pos :int)))
(define-beep-function activate)

(defun get-volume (session)
  (with-foreign-objects
      ((l :int) (r :int))
    (%get-volume session l r)
    (values (deref-pointer l :int)
	    (deref-pointer r :int))))

(defun get-info (session)
  (with-foreign-objects
      ((rate :int) (freq :int) (nch :int))
    (%get-info session rate freq nch)
    (values (deref-pointer rate :int)
	    (deref-pointer freq :int)
	    (deref-pointer nch :int))))

;;;; beep-uffi.lisp ends here
