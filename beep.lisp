;;;; -*- mode: lisp; package: beep -*-

;;;; Interface to the Beep Media Player
;;;; Copyright (C) 2004 Matthew Kennedy <mkennedy@gentoo.org>

;;;; Commentary:

;;;; Code:

(in-package #:beep)

(defvar *default-handle* 0
  "Default remote Beep Media Player session handle.")

(defun int->bool (n)
  (if (zerop n) nil t))

(defun bool->int (n)
  (if n 1 0))

(defmacro define-beep-function (name args &body body)
  (let ((new-args (append args 
			  (if (eq (car args) '&optional)
			      '((id *default-handle*))
			      '(&optional (id *default-handle*))))))
    `(defun ,name ,new-args
       ,@body)))

(define-beep-function version ()
  (beep-uffi:get-version id))

(define-beep-function play ()
  (beep-uffi:play id))

(define-beep-function pause ()
  (beep-uffi:pause id))

(define-beep-function playing-p ()
  (int->bool (beep-uffi:is-playing id)))

(define-beep-function paused-p ()
  (int->bool (beep-uffi:is-paused (int->bool id))))

(define-beep-function playlist-position ()
  (beep-uffi:get-playlist-pos id))


(defsetf playlist-position (&optional (id *default-handle*)) (position)
  `(beep-uffi:set-playlist-pos ,id ,position))

(define-beep-function playlist-length ()
  (beep-uffi:get-playlist-length id))

(define-beep-function playlist-clear ()
  (beep-uffi:playlist-clear id))

(define-beep-function output-time ()
  (beep-uffi:get-output-time id))

(define-beep-function jump-to-time ( time)
  (beep-uffi:jump-to-time id time))

(define-beep-function volume ()
  (beep-uffi:get-volume id))

(defsetf volume (&optional (id *default-handle*)) (new-volume)
  (let ((level (gensym)))
    `(let ((,level ,new-volume))
       (beep-uffi:set-volume ,id ,level ,level))))

(defsetf volume-left (&optional (id *default-handle*)) (level)
  (let ((handle (gensym)))
    `(let ((,handle ,id))
       (multiple-value-bind (left right)
	   (volume ,handle)
       (declare (ignore left))
       (beep-uffi:set-volume ,handle ,level right)))))

(defsetf volume-right (&optional (id *default-handle*)) (level)
  (let ((handle (gensym)))
    `(let ((,handle ,id))
       (multiple-value-bind (left right)
	   (volume ,handle)
       (declare (ignore right))
       (beep-uffi:set-volume ,handle left ,level)))))

(define-beep-function main-volume ()
  (beep-uffi:get-main-volume id))

(defsetf main-volume (&optional (id *default-handle*)) (level)
  `(beep-uffi:set-main-volume ,id ,level))

(define-beep-function balance ()
  (beep-uffi:get-balance id))

(defsetf balance (&optional (id *default-handle*)) (balance)
  `(beep-uffi:set-balance ,id ,balance))

(define-beep-function skin ()
  (beep-uffi:get-skin id))

(defsetf skin (&optional (id *default-handle*)) (skin)
  `(beep-uffi:set-skin ,id ,skin))

(define-beep-function playlist-file (&optional (position 0))
  (beep-uffi:get-playlist-file id position))

(define-beep-function playlist-title (&optional (position 0))
  (beep-uffi:get-playlist-title id position))

(define-beep-function playlist-time (&optional (position 0)) 
  (beep-uffi:get-playlist-time id position))

(define-beep-function details ()
  (beep-uffi:get-info id))

(define-beep-function show-main-window (show)
  (beep-uffi:main-win-toggle id (bool->int show)))

(define-beep-function show-playlist (show)
  (beep-uffi:pl-win-toggle id (bool->int show)))

(define-beep-function show-equalizer (show)
  (beep-uffi:eq-win-toggle id (bool->int show)))

(define-beep-function show-preferences ()
  (beep-uffi:show-prefs-box id))

(define-beep-function show-always-on-top (show)
  (beep-uffi:toggle-aot id (bool->int show)))

(define-beep-function eject ()
  (beep-uffi:eject id))

(define-beep-function playlist-next ()
  (beep-uffi:playlist-next id))

(define-beep-function playlist-previous ()
  (beep-uffi:playlist-prev id))

(define-beep-function playlist-append-url ( url)
  (beep-uffi:playlist-add-url-string id url))

(define-beep-function running-p ()
  (int->bool (beep-uffi:is-running id)))

(define-beep-function repeat-state ()
  (int->bool (beep-uffi:is-repeat id)))

(defsetf repeat-state (&optional (id *default-handle*)) (new-repeat-p)
  (let ((repeat (gensym)))
    `(let ((,repeat (repeat-state ,id)))
       (unless (eql ,repeat ,new-repeat-p)
	 (beep-uffi:toggle-repeat ,id)))))

(define-beep-function shuffle-state ()
  (int->bool (beep-uffi:is-shuffle id)))

(defsetf shuffle-state (&optional (id *default-handle*)) (new-shuffle-p)
  (let ((shuffle (gensym)))
    `(let ((,shuffle (shuffle-state ,id)))
       (unless (eql ,shuffle ,new-shuffle-p)
	 (beep-uffi:toggle-shuffle ,id)))))

(define-beep-function equalizer-preamp ()
  (beep-uffi:get-eq-preamp id))

(defsetf equalizer-preamp (level &optional (id *default-handle*)) (new-level)
  (declare (ignore level))
  `(beep-uffi:set-eq-preamp ,id ,new-level))

(define-beep-function equalizer-band (band)
  (beep-uffi:get-eq-band id band))

(defsetf equalizer-band (band &optional (id *default-handle*)) (value)
  `(beep-uffi:set-eq-band ,id ,band ,value))

(define-beep-function quit ()
  (beep-uffi:quit id))

(define-beep-function play-pause ()
  (beep-uffi:play-pause id))

(define-beep-function playlist-insert-url ( url position)
  (beep-uffi:playlist-ins-url-string id url position))

(define-beep-function activate ()
  (beep-uffi:activate id))

;;;; beep.lisp ends here
