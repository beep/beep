;;;; -*- mode: lisp; package: beep -*-

;;;; Interface to the Beep Media Player
;;;; Copyright (C) 2004 Matthew Kennedy <mkennedy@gentoo.org>

;;;; Commentary:

;;;; Code:

(in-package #:common-lisp)

(defpackage #:beep-uffi
  (:use #:common-lisp
	#:uffi)
  (:export #:get-version
	   #:play
	   #:pause
	   #:stop
	   #:is-playing
	   #:is-paused
	   #:get-playlist-pos
	   #:set-playlist-pos
	   #:get-playlist-length
	   #:playlist-clear
	   #:get-output-time
	   #:jump-to-time
	   #:get-volume
	   #:get-main-volume
	   #:get-balance
	   #:set-volume
	   #:set-main-volume
	   #:set-balance
	   #:get-skin
	   #:set-skin
	   #:get-playlist-file
	   #:get-playlist-title
	   #:get-playlist-time
	   #:get-info
	   #:main-win-toggle
	   #:pl-win-toggle
	   #:eq-win-toggle
	   #:show-prefs-box
	   #:toggle-aot
	   #:eject
	   #:playlist-prev
	   #:playlist-next
	   #:playlist-add-url-string
	   #:is-running
	   #:toggle-repeat
	   #:toggle-shuffle
	   #:is-repeat
	   #:is-shuffle
	   #:get-eq-preamp
	   #:get-eq-band
	   #:set-eq-preamp
	   #:set-eq-band
	   #:quit
	   #:play-pause
	   #:playlist-ins-url-string
	   #:activate))

(defpackage #:beep 
  (:use #:common-lisp)
  (:export #:session
	   #:version
	   #:play
	   #:pause
	   #:playing-p
	   #:paused-p
	   #:playlist-position
	   #:playlist-length
	   #:clear-playlist
	   #:output-time
	   #:jump-to-time
	   #:volume
	   #:volume-left
	   #:volume-right
	   #:main-volume
	   #:balance
	   #:skin
	   #:playlist-file
	   #:playlist-title
	   #:playlist-time
	   #:details
	   #:show-main-window
	   #:show-playlist
	   #:show-equalizer
	   #:show-preferences
	   #:show-always-on-top
	   #:eject
	   #:playlist-next
	   #:playlist-previous
	   #:playlist-append-url
	   #:running-p
	   #:repeat-state
	   #:shuffle-state
	   #:equalizer-preamp
	   #:equalizer-band
	   #:quit
	   #:play-pause
	   #:playlist-insert-url
	   #:activate))

;;;; packages.lisp ends here
