;;;; -*- mode: lisp; package: beep-system -*-

;;;; Interface to the Beep Media Player
;;;; Copyright (C) 2004 Matthew Kennedy <mkennedy@gentoo.org>

;;;; Commentary:

;;;; Code:

(in-package #:common-lisp)

(defpackage #:beep-system 
  (:use #:common-lisp
	#:asdf))

(in-package #:beep-system)

(defsystem #:beep
    :components ((:file "packages")
		 (:file "uffi" :depends-on ("packages"))
		 (:file "beep-uffi" :depends-on ("uffi"))
		 (:file "beep" :depends-on ("beep-uffi")))
    :depends-on (#:uffi))

;;;; beep.asd ends here
