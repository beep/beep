;;;; -*- mode: lisp; package: beep -*-

;;;; Interface to the Beep Media Player
;;;; Copyright (C) 2004 Matthew Kennedy <mkennedy@gentoo.org>

;;;; Commentary:

;;;; Code:

(in-package #:beep-uffi)

(defvar *foreign-libraries*
  (mapcar #'(lambda (names)
	      (or (find-foreign-library names '("/usr/lib/" "/usr/local/lib"))
		  (error "Unable to find foreign library corresponding to ~S" names)))
	  '("libgconf-2" "libbeep"))
  "Libraries to be loaded.")

(defvar *supporting-libraries* '()
  "Other libraries dependencies.")

(defvar *uffi-loaded* nil
  "Non-NIL if the library is already loaded.")

(defun maybe-load-library ()
  "Returns T if the library was loaded successfully or
NIL if the library has already been loaded."
  (unless *uffi-loaded*
    (dolist (library *foreign-libraries*)
      (unless (load-foreign-library 
	       library 
	       :module "beep" 
	       :supporting-libraries *supporting-libraries*)
	(error "Unable to load foreign library ~A" library)))
    (setf *uffi-loaded* t)))

(maybe-load-library)

;;;; uffi.lisp ends here
